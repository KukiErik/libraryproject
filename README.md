# Library

## What you need

* Angular 2+ (recomended v. 11)
* .NET Core 3.1 https://dotnet.microsoft.com/download
* PostgreSQL Database https://www.enterprisedb.com/downloads/postgres-postgresql-downloads

# Getting Started

## Preparation

* Make sure your system has installed Node.js. node.js https://nodejs.org/en/download/
* Open a terminal window and go to the Sources directory.
* Run the npm install command. This command will load all necessary dependencies specified in the package.json file.
* Then run the npm install -g @angular/cli command to install Angular CLI (if not yet installed).

## Build Project - FrontEnd
To start a local HTTP server and runs the build process with change tracking run the command:
```
ng serve
or
npm start
```

## Build Project - BackEnd
You need Visual Studio to load the project.

## Build Project - Database
PostgreSQL Database -> Create new Database named Library then run the script in folder libraryDB named createTable.sql.

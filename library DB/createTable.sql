CREATE TABLE "Books" (
	book_id SERIAL,
	author VARCHAR,
	book_title VARCHAR,
	description TEXT,
	CONSTRAINT book_id_pkey PRIMARY KEY (book_id)
);

CREATE TABLE "Borrowed" (
	borrowed_id SERIAL,
	book_id INTEGER,
	loan_date DATE,
	first_name VARCHAR,
	last_name VARCHAR,
	planned_return DATE,
	actual_return DATE,
	CONSTRAINT borrowed_id_pkey PRIMARY KEY (borrowed_id),
	CONSTRAINT knjige_id
		FOREIGN KEY (book_id)
			REFERENCES "Books" (book_id) MATCH SIMPLE
			ON UPDATE NO ACTION 
			ON DELETE NO ACTION
);
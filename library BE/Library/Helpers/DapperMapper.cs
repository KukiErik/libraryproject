﻿using Dapper;
using Models;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Helpers
{
    public class DapperMapper
    {
        public static CustomPropertyTypeMap getBooks()
        {
            Dictionary<string, string> columnMaps = new Dictionary<string, string>
            {
                // Column => Proprety
                {"book_id", "Id" },
                {"author", "Author" },
                {"book_title", "Title" },
                {"description", "Description" },
            };

            // Column name mapping function
            var mapper = new Func<Type, string, PropertyInfo>((type, columnName) =>
            {
                if (columnMaps.ContainsKey(columnName))
                    return type.GetProperty(columnMaps[columnName]);
                else
                    return type.GetProperty(columnName);
            });
            var BookMap = new CustomPropertyTypeMap(typeof(BookModel), (type, columnName) => mapper(type, columnName));

            return BookMap;
        }

        public static CustomPropertyTypeMap getBorrowedBooks()
        {
            Dictionary<string, string> columnMaps = new Dictionary<string, string>
            {
                // Column => Proprety
                {"borrowed_id", "Id" },
                {"book_id", "BookId" },
                {"loan_date", "LoanDate" },
                {"first_name", "FirstName" },
                {"last_name", "LastName" },
                {"planned_return", "PlannedReturn" },
                {"actual_return", "ActualReturn" },
            };

            // Column name mapping function
            var mapper = new Func<Type, string, PropertyInfo>((type, columnName) =>
            {
                if (columnMaps.ContainsKey(columnName))
                    return type.GetProperty(columnMaps[columnName]);
                else
                    return type.GetProperty(columnName);
            });
            var BorrowedBookMap = new CustomPropertyTypeMap(typeof(BorrowedBookModel), (type, columnName) => mapper(type, columnName));

            return BorrowedBookMap;
        }
    }
}

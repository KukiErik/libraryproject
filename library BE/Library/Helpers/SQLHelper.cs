﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Helpers
{
    public class SQLHelper
    {

        // -------------------- BOOK -----------------------------

        public static string QueryGetAllBooks()
        {
            return "SELECT * FROM public.\"Books\"";
        }

        public static string QueryBookByTitle()
        {
            return QueryGetAllBooks() + "WHERE book_title = @title";
        }

        public static string QueryGetBookById()
        {
            return QueryGetAllBooks() + "WHERE book_id=@id";
        }

        public static string QueryDelete()
        {
            return "DELETE FROM public.\"Books\" WHERE book_id=@Id";
        }

        public static string QueryAddBook()
        {
            return "INSERT INTO public .\"Books\"" +
                "(author, book_title, description) " +
                "VALUES(@Author, @Title, @Description) " +
                "RETURNING book_id";
        }

        // -------------------- LOAN -----------------------------

        public static string QueryGetAllLoan()
        {
            return "SELECT * FROM public.\"Borrowed\"";
        }

        public static string QueryAddNewLoan()
        {
            return "INSERT INTO public.\"Borrowed\"" +
                "(loan_date, first_name, last_name, planned_return, actual_return, book_id) " +
                "VALUES(@LoanDate, @FirstName, @LastName, @PlannedReturn, @ActualReturn, @BookId) " +
                "RETURNING borrowed_id";
        }

        public static string QueryGetAllLoanById()
        {
            return QueryGetAllLoan() + "WHERE borrowed_id = @id";
        }

        public static string QueryUpdateLoan()
        {
            return " UPDATE public.\"Borrowed\" " +
                " SET book_id = @BookId, loan_date = @LoanDate, first_name = @FirstName, last_name = @LastName, planned_return = @PlannedReturn, actual_return = @ActualReturn " +
                " WHERE (borrowed_id = @Id) ";
        }
    }
}

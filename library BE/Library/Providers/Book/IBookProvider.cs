﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Providers.Book
{
    public interface IBookProvider
    {
        /// <summary>
        /// This method returns all the books in database.
        /// </summary>
        /// <returns>
        /// All books.
        /// </returns>
        Task<IEnumerable<BookModel>> getBooks();

        /// <summary>
        ///     This method returns books with the given title.
        /// </summary>
        /// <returns>
        ///     Book by title if exists, or else null.
        /// </returns>
        /// <param name="id"> Product's id </param>
        Task<BookModel> GetBook(string title);

        /// <summary>
        ///     This method adds a new book.
        /// </summary>
        /// <param name="book"></param>
        Task Add(BookModel book);

        /// <summary>
        ///     This method retuns books with the given id.
        /// </summary>
        /// <returns>
        ///     Book by id if exists, or else null.
        /// </returns>
        /// <param name="id"> Book's id </param>
        Task<BookModel> GetBook(long id);

        /// <summary>
        ///     This method removes a book with the given id.
        /// </summary>
        /// <param name="id"> Book's id </param>
        Task DeleteBook(long id);
    }
}

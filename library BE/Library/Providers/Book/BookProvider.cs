﻿using Dapper;
using Helpers;
using Microsoft.Extensions.Configuration;
using Models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Providers.Book
{
    public class BookProvider : IBookProvider
    {
        private readonly IConfiguration _configuration;

        public BookProvider(IConfiguration configuration)
        {
            _configuration = configuration ?? throw new NullReferenceException(nameof(configuration));
        }
        private IDbConnection Connection
        {
            get
            {
                SqlMapper.SetTypeMap(typeof(BookModel), DapperMapper.getBooks());

                return new NpgsqlConnection(_configuration.GetConnectionString("DataBase"));
            }
        }

        public async Task<IEnumerable<BookModel>> getBooks()
        {
            using (IDbConnection connection = Connection)
            {
                connection.Open();

                var res = await connection.QueryAsync<BookModel>(SQLHelper.QueryGetAllBooks());

                return res;
            }
        }


        public async Task<BookModel> GetBook(string title)
        {
            using (IDbConnection connection = Connection)
            {
                connection.Open();

                var res = await connection.QueryFirstOrDefaultAsync<BookModel>(SQLHelper.QueryBookByTitle(), new { Title = title });

                return res;
            }

        }

        public async Task Add(BookModel book)
        {
            using (IDbConnection connection = Connection)
            {
                connection.Open();

                await connection.ExecuteAsync(SQLHelper.QueryAddBook(), book);
            }
        }

        public async Task<BookModel> GetBook(long id)
        {
            using (IDbConnection connection = Connection)
            {
                connection.Open();

                var res = await connection.QueryFirstOrDefaultAsync<BookModel>(SQLHelper.QueryGetBookById(), new { Id = id });

                return res;
            }
        }

        public async Task DeleteBook(long id)
        {
            using (IDbConnection connection = Connection)
            {
                connection.Open();

                var res = await connection.ExecuteAsync(SQLHelper.QueryDelete(), new { Id = id });
            }
        }
    }
}

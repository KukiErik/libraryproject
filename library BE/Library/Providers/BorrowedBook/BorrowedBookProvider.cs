﻿using Dapper;
using Helpers;
using Microsoft.Extensions.Configuration;
using Models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Providers.BorrowedBook
{
    public class BorrowedBookProvider : IBorrowedBookProvider
    {
        private readonly IConfiguration _configuration;

        public BorrowedBookProvider(IConfiguration configuration)
        {
            _configuration = configuration ?? throw new NullReferenceException(nameof(configuration));
        }
        private IDbConnection Connection
        {
            get
            {
                SqlMapper.SetTypeMap(typeof(BorrowedBookModel), DapperMapper.getBorrowedBooks());

                return new NpgsqlConnection(_configuration.GetConnectionString("DataBase"));
            }
        }

        public async Task<IEnumerable<BorrowedBookModel>> getBorroweds()
        {
            using (IDbConnection connection = Connection)
            {
                connection.Open();

                var res = await connection.QueryAsync<BorrowedBookModel>(SQLHelper.QueryGetAllLoan());

                return res;
            }
        }

        public async Task AddBorrow(BorrowedBookModel borrowedBookModel)
        {
            using (IDbConnection connection = Connection)
            {
                connection.Open();

                await connection.ExecuteAsync(SQLHelper.QueryAddNewLoan(), borrowedBookModel);
            }
        }

        public async Task<BorrowedBookModel> getBorroweds(long id)
        {
            using (IDbConnection connection = Connection)
            {
                connection.Open();

                var res = await connection.QueryFirstOrDefaultAsync<BorrowedBookModel>(SQLHelper.QueryGetAllLoanById(), new { Id = id });

                return res;
            }
        }

        public async Task UpdateLoan(BorrowedBookModel borrowedBookModel)
        {
            using (IDbConnection connection = Connection)
            {
                connection.Open();

                await connection.ExecuteAsync(SQLHelper.QueryUpdateLoan(), borrowedBookModel);
            }
        }
    }
}

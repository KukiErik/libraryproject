﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Providers.BorrowedBook
{
    public interface IBorrowedBookProvider
    {
        /// <summary>
        ///     This method returns all the borrowed books in database.
        /// </summary>
        /// <returns>
        ///     All borrowed books.
        /// </returns>
        Task<IEnumerable<BorrowedBookModel>> getBorroweds();

        /// <summary>
        ///     This method adds a new loan.
        /// </summary>
        /// <param name="borrowedBookModel"></param>
        Task AddBorrow(BorrowedBookModel borrowedBookModel);

        /// <summary>
        ///     This method retuns a loan with the given id.
        /// </summary>
        /// <returns>
        ///     Loan by id if exists, or else null.
        /// </returns>
        /// <param name="id"> Book's id </param>
        Task<BorrowedBookModel> getBorroweds(long id);

        /// <summary>
        ///     This method updates a loan.
        /// </summary>
        /// <param name="borrowedBookModel"> Changed loan </param>
        Task UpdateLoan(BorrowedBookModel borrowedBookModel);
    }
}

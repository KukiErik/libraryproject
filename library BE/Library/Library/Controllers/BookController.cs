﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Models;
using Services.Book;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Controllers
{
    /// <summary>
    ///     Provides methods for geting, updating and deleting books.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly IBookService _bookService;
        private readonly ILogger _logger;

        public BookController(IBookService bookService, ILoggerFactory logger)
        {
            _bookService = bookService ?? throw new NullReferenceException(nameof(bookService));
            _logger = logger.CreateLogger<BookController>();
        }

        
        /// <summary>
        ///     Findes all the books in database.
        /// </summary>
        /// <returns>
        ///     All books.
        /// </returns>
        //GET: api/book
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BookModel>>> getBooks()
        {
            try
            {
                var book = await _bookService.getBooks();
                return Ok(book);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception in getBooks()");
                return StatusCode(500);
            }
        }

        /// <summary>
        ///     Finds and returns books by id. 
        /// </summary>
        /// <param name="id"> Book id </param>
        /// <returns>
        ///     Book
        /// </returns>
        // POST: api/book/id
        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult<BookModel>> GetBook(long id)
        {
            try
            {
                var book = await _bookService.GetBook(id);

                if (book == null)
                    return NotFound(new { message = "Book could not be found." });

                return Ok(book);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception in GetBookById()");

                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        ///     Adds new book.
        /// </summary>
        /// <param name="bookModel"> New Book </param>
        /// <returns>
        ///     Status 200(Ok)/500(Error)
        /// </returns>
        // POST: api/book
        [HttpPost]
        public async Task<ActionResult> AddBook(BookModel book)
        {
            try
            {
                var bookN = await _bookService.Get(book.Title);

                if (bookN != null)
                    return BadRequest(new { message = "Book already exists." });

                await _bookService.AddBook(book);

                return Ok(new { message = "Book was succesfully added." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception in AddBook()");

                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        ///     Finds and removes book by id. 
        /// </summary>
        /// <param name="id"> Book id </param>
        /// <returns>
        ///     Status 200(Ok)/404(NotFound)/500(Error)
        /// </returns>
        // POST: api/book/id
        [HttpDelete]
        [Route("{id}")]
        public async Task<ActionResult<BookModel>> DeleteBook(long id)
        {
            try
            {
                var book = await _bookService.GetBook(id);

                if (book == null)
                    return NotFound(id);

                await _bookService.Delete(id);

                return Ok(new { message = "Book has been deleted." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception in DeleteBook()");

                return StatusCode(500, ex.Message);
            }
        }
    }
}

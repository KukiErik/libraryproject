﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Models;
using Services.BorrowedBook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Controllers
{
    /// <summary>
    ///     Provides methods for geting, updating and deleting books.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class BorrowedBookController : ControllerBase
    {
        private readonly IBorrowedBookService _borrowedBookService;
        private readonly ILogger _logger;

        public BorrowedBookController(IBorrowedBookService borrowedBookService, ILoggerFactory logger)
        {
            _borrowedBookService = borrowedBookService ?? throw new NullReferenceException(nameof(borrowedBookService));
            _logger = logger.CreateLogger<BorrowedBookController>();
        }

        /// <summary>
        ///     Findes all loans in database.
        /// </summary>
        /// <returns>
        ///     All loans.
        /// </returns>
        //GET: api/borrowedbook
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BorrowedBookModel>>> getBorroweds()
        {
            try
            {
                var loan = await _borrowedBookService.getBorroweds();
                return Ok(loan);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception in getBooks()");
                return StatusCode(500);
            }
        }

        /// <summary>
        ///     Finds and returns loans by id. 
        /// </summary>
        /// <param name="id"> Loan id </param>
        /// <returns>
        ///     Loan
        /// </returns>
        // POST: api/BorroweBook/id
        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult<BookModel>> getBorroweds(long id)
        {
            try
            {
                var loan = await _borrowedBookService.getBorroweds(id);

                if (loan == null)
                    return NotFound(new { message = "Loan could not be found." });

                return Ok(loan);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception in getBorrowedsById()");

                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        ///     Adds new loan.
        /// </summary>
        /// <param name="bookModel"> New Loan </param>
        /// <returns>
        ///     Status 200(Ok)/500(Error)
        /// </returns>
        // POST: api/borrowedbook
        [HttpPost]
        public async Task<ActionResult> AddBorrow(BorrowedBookModel borrowedBookModel)
        {
            try
            {
                await _borrowedBookService.AddBorrow(borrowedBookModel);

                return Ok(new { message = "Loan was succesfully created." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception in AddBook()");

                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        ///     Adds new loan.
        /// </summary>
        /// <param name="bookModel"> New Loan </param>
        /// <returns>
        ///     Status 200(Ok)/500(Error)
        /// </returns>
        // POST: api/BorrowedBook
        [HttpPut]
        public async Task<ActionResult> UpdateBorrowed(BorrowedBookModel borrowedBookModel)
        {
            try
            {
                var res = await _borrowedBookService.getBorroweds(borrowedBookModel.Id);

                if (res == null)
                    return NotFound(borrowedBookModel);

                await _borrowedBookService.UpdateLoan(borrowedBookModel);

                return Ok(new { message = "Loan is now up to date." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception in UpdateBorrowed()");

                return StatusCode(500, ex.Message);
            }
        }
    }
}

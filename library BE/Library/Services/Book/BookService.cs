﻿using Models;
using Providers.Book;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Book
{
    public class BookService : IBookService
    {
        private readonly IBookProvider _bookProvider;

        public BookService(IBookProvider bookProvider)
        {
            _bookProvider = bookProvider ?? throw new NullReferenceException(nameof(bookProvider));
        }

        public async Task<IEnumerable<BookModel>> getBooks()
        {
            return await _bookProvider.getBooks();
        }

        public async Task<BookModel> Get(string title)
        {
            return await _bookProvider.GetBook(title);
        }

        public async Task AddBook(BookModel book)
        {
            await _bookProvider.Add(book);
        }

        public async Task<BookModel> GetBook(long id)
        {
            return await _bookProvider.GetBook(id);
        }

        public async Task Delete(long id)
        {
            await _bookProvider.DeleteBook(id);
        }
    }
}

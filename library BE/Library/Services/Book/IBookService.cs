﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Book
{
    public interface IBookService
    {
        Task<IEnumerable<BookModel>> getBooks();

        Task<BookModel> Get(string title);

        Task AddBook(BookModel book);

        Task<BookModel> GetBook(long id);

        Task Delete(long id);
    }
}

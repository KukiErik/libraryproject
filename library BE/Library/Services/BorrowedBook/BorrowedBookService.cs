﻿using Models;
using Providers.BorrowedBook;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.BorrowedBook
{
    public class BorrowedBookService : IBorrowedBookService
    {
        private readonly IBorrowedBookProvider _borrowedBookProvider;

        public BorrowedBookService(IBorrowedBookProvider borrowedBookProvider)
        {
            _borrowedBookProvider = borrowedBookProvider ?? throw new NullReferenceException(nameof(borrowedBookProvider));
        }

        public async Task<IEnumerable<BorrowedBookModel>> getBorroweds()
        {
            return await _borrowedBookProvider.getBorroweds();
        }

        public async Task AddBorrow(BorrowedBookModel borrowedBookModel)
        {
            await _borrowedBookProvider.AddBorrow(borrowedBookModel);
        }

        public async Task<BorrowedBookModel> getBorroweds(long id)
        {
            return await _borrowedBookProvider.getBorroweds(id);
        }

        public async Task UpdateLoan(BorrowedBookModel borrowedBookModel)
        {
            await _borrowedBookProvider.UpdateLoan(borrowedBookModel);
        }
    }
}

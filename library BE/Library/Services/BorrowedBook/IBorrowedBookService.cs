﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.BorrowedBook
{
    public interface IBorrowedBookService
    {
        Task<IEnumerable<BorrowedBookModel>> getBorroweds();

        Task AddBorrow(BorrowedBookModel bbook);

        Task<BorrowedBookModel> getBorroweds(long id);

        Task UpdateLoan(BorrowedBookModel borrowedBookModel);
    }
}

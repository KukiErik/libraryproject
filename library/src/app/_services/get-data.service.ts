import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Books } from '../_models/books';
import { BooksComponent } from '../_components/books/books.component';
import { LoanArchive } from '../_models/loanArchive';

@Injectable({
  providedIn: 'root',
})
export class GetDataService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getAlBooks(): Promise<Books[]> {
    return this.http
      .get<Books[]>(`${this.apiUrl}/book`)
      .toPromise()
      .then((res) => res);
  }

  addBook(book: any): Promise<any> {
    return this.http
      .post(`${this.apiUrl}/book`, book)
      .toPromise()
      .then((res) => res);
  }

  getBookById(id: number) {
    return this.http.get<Books>(`${this.apiUrl}/book/${id}`);
  }

  deleteBook(id: number) {
    return this.http.delete(`${this.apiUrl}/book/${id}`);
  }

  getAllLoans(): Promise<LoanArchive[]> {
    return this.http
      .get<LoanArchive[]>(`${this.apiUrl}/BorrowedBook`)
      .toPromise()
      .then((res) => res);
  }

  newLoan(loanArchive: any): Promise<any> {
    return this.http
      .post(`${this.apiUrl}/BorrowedBook`, loanArchive)
      .toPromise()
      .then((res) => res);
  }

  updateLoan(loanArchive: any): Promise<any> {
    return this.http
      .put(`${this.apiUrl}/BorrowedBook`, loanArchive)
      .toPromise()
      .then((res) => res);
  }
}

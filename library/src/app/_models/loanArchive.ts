export class LoanArchive {
  id!: number;
  bookId!: number;
  loanDate!: Date;
  firstName!: string;
  lastName!: string;
  plannedReturn!: Date;
  actualReturn!: Date;
}

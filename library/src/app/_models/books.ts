export class Books {
    id!: number;
    author!: string;
    title!: string;
    description!: string;
}
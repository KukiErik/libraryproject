import { Component, Inject, OnInit } from '@angular/core';
import { Books } from 'src/app/_models/books';
import { LoanArchive } from 'src/app/_models/loanArchive';
import { GetDataService } from 'src/app/_services/get-data.service';
import { formatDate } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { DialogAddBook } from '../books/books.component';

@Component({
  selector: 'app-loan-archive',
  templateUrl: './loan-archive.component.html',
  styleUrls: ['./loan-archive.component.scss'],
})
export class LoanArchiveComponent implements OnInit {
  loanArchives!: LoanArchive[];
  books!: Books[];
  loanUser!: LoanArchive;
  currentUser!: LoanArchive;
  today = new Date();

  searchField!: string;
  clearSearchField() {
    this.searchField = '';
  }

  constructor(private dataService: GetDataService, public dialog: MatDialog) {}

  ngOnInit(): void {
    this.getAllLoans();
    this.getAllBooks();
  }

  getAllLoans() {
    this.dataService
      .getAllLoans()
      .then((loanArchive) => (this.loanArchives = loanArchive))
      .then((_) => console.log(this.loanArchives));
  }

  getAllBooks() {
    this.dataService
      .getAlBooks()
      .then((books) => (this.books = books))
      .then((_) => console.log(this.books));
  }

  setCurrentUser(loanUser: LoanArchive) {
    this.currentUser = loanUser;
  }

  openDialog() {
    this.dialog.open(DialogLoanUpdate, {
      data: {
        id: this.currentUser.id,
        bookId: this.currentUser.bookId,
        firstName: this.currentUser.firstName,
        lastName: this.currentUser.lastName,
        plannedReturn: this.currentUser.plannedReturn,
        actualReturn: this.currentUser.actualReturn,
      },
    });
  }

  updateLoan(loanArchive: LoanArchive) {
    this.currentUser = loanArchive;
    this.openDialog();
  }
}

@Component({
  selector: 'dialog-loan-update',
  templateUrl: './dialog-loan-update.html',
  styleUrls: ['./dialog-loan-update.scss'],
})
export class DialogLoanUpdate {
  returnForm!: FormGroup;
  submittedR = false;

  constructor(
    private formBuilder: FormBuilder,
    private dataService: GetDataService,
    public dialogRef: MatDialogRef<DialogAddBook>,
    @Inject(MAT_DIALOG_DATA) public data: LoanArchive
  ) {
    dialogRef.disableClose = true;
    this.returnForm = this.formBuilder.group({
      actualReturn: [Date, Validators.required],
    });
  }

  onSubmitReturn() {
    if (this.returnForm.valid) {
      const tmp = this.returnForm.value;
      const loanArchive = {
        id: this.data.id,
        bookId: this.data.bookId,
        loanDate: this.data.loanDate,
        firstName: this.data.firstName,
        lastName: this.data.lastName,
        plannedReturn: this.data.plannedReturn,
        actualReturn: tmp.actualReturn,
      };
      console.log(loanArchive);
      this.dataService.updateLoan(loanArchive).then((x) => {
        this.returnForm.reset();
        this.submittedR = false;
        this.dialogRef.close();
      });
    } else {
      this.submittedR = true;
      console.log('invalid!');
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}

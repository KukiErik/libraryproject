import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanArchiveComponent } from './loan-archive.component';

describe('LoanArchiveComponent', () => {
  let component: LoanArchiveComponent;
  let fixture: ComponentFixture<LoanArchiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoanArchiveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanArchiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

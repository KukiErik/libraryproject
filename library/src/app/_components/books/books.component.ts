import { Component, Inject, Input, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatCard } from '@angular/material/card';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Books } from 'src/app/_models/books';
import { LoanArchive } from 'src/app/_models/loanArchive';
import { GetDataService } from 'src/app/_services/get-data.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss'],
})
export class BooksComponent implements OnInit {
  books!: Books[];
  currentBook!: Books;

  searchField!: string;
  clearSearchField() {
    this.searchField = '';
  }

  constructor(private dataService: GetDataService, public dialog: MatDialog) {}

  openDialog() {
    this.dialog.open(DialogAddBook);
  }

  openDialogLoan() {
    this.dialog.open(DialogNewLoan, {
      data: {
        bookId: this.currentBook.id,
      },
    });
  }

  ngOnInit(): void {
    this.getAllBooks();
  }

  getAllBooks() {
    this.dataService
      .getAlBooks()
      .then((books) => (this.books = books))
      .then((_) => console.log(this.books));
  }

  setCurrentBook(book: Books) {
    this.currentBook = book;
  }

  newLoan(book: Books) {
    this.currentBook = book;
    this.openDialogLoan();
  }

  public deleteBook(book: Books): void {
    this.dataService.deleteBook(book.id).subscribe((res) => this.getAllBooks());
    this.books = this.books.filter((b) => b.id !== book.id);
    console.log('Deleted book: ' + book.id + ' title: ' + book.title);
    //this.getAllBooks();
  }
}

@Component({
  selector: 'dialog-add-book',
  templateUrl: './dialog-add-book.html',
  styleUrls: ['./dialog-add-book.scss'],
})
export class DialogAddBook {
  books!: Books[];

  newBookForm!: FormGroup;
  submittedB = false;

  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  authorFormControler = new FormControl('', [Validators.required]);

  constructor(
    private snackbar: MatSnackBar,
    private formBuilder: FormBuilder,
    private dataService: GetDataService,
    public dialogRef: MatDialogRef<DialogAddBook>,
    @Inject(MAT_DIALOG_DATA) public data: Books
  ) {
    dialogRef.disableClose = true;
    this.newBookForm = this.formBuilder.group({
      author: ['', Validators.required],
      title: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  openSnackBar() {
    this.snackbar.open('Book successfully added! ', ' X ', {
      duration: 1000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }

  onSubmitBook() {
    if (this.newBookForm.valid) {
      const tmp = this.newBookForm.value;
      const book = {
        author: tmp.author,
        title: tmp.title,
        description: tmp.description,
      };
      console.log(book);
      this.dataService.addBook(book).then((x) => {
        this.newBookForm.reset();
        this.submittedB = false;
        this.dialogRef.close();
        this.openSnackBar();
      });
    } else {
      this.submittedB = true;
      console.log('invalid!');
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'dialog-new-loan',
  templateUrl: './dialog-new-loan.html',
  styleUrls: ['./dialog-new-loan.scss'],
})
export class DialogNewLoan {
  books!: Books[];
  loan!: LoanArchive[];
  newLoanForm!: FormGroup;
  submittedL = false;
  date = new Date();
  @Input() item!: number;
  nullDate = '0001-01-01';

  constructor(
    private formBuilder: FormBuilder,
    private dataService: GetDataService,
    public dialogRef: MatDialogRef<DialogAddBook>,
    @Inject(MAT_DIALOG_DATA) public data: Books,
    @Inject(MAT_DIALOG_DATA) public bookId: { bookId: number }
  ) {
    dialogRef.disableClose = true;
    this.newLoanForm = this.formBuilder.group({
      bookId: [Number],
      loanDate: [Date],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      plannedReturn: [Date, Validators.required],
    });
  }

  onSubmitLoan() {
    if (this.newLoanForm.valid) {
      const tmp = this.newLoanForm.value;
      const loanArchive = {
        bookId: this.bookId.bookId,
        loanDate: this.date,
        firstName: tmp.firstName,
        lastName: tmp.lastName,
        plannedReturn: tmp.plannedReturn,
        actualReturn: this.nullDate,
      };
      console.log(loanArchive);
      this.dataService.newLoan(loanArchive).then((x) => {
        this.newLoanForm.reset();
        this.submittedL = false;
        this.dialogRef.close();
      });
    } else {
      this.submittedL = true;
      console.log('invalid!');
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
